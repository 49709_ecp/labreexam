#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct box{
    int number;
    int weight;
    int height;
    int width;
    char colour[10];

}box_t;

void accept_box(box_t *b1);
void accept_use(box_t*b1);
void print_box(box_t *b1);
void print_use(box_t *b1);
void delete_box( const char *path, int id );
void read_record( const char *path);
void write_add_box( const char *path);
int** allocate_memory( );


int menu_list(void)
{
    int choice;
    printf("0.exits\n");
    printf("1.add box\n");
    printf("2.delete box\n");
    printf("enter the choice\n");
    scanf("%d",&choice);
    return choice;
}

int main (void)
{

    int choice;
    box_t box1[10];
    // memory aalocation 
    int **b = allocate_memory( );
    while( (choice = menu_list()) != 0)
{
    switch(choice)
    {
        case 1:
        write_add_box("File.dat");
        break;
        case 2:
        delete_box("File.dat", 9);
        break;

    }
}
    return 0;

}


void accept_box(box_t *b1){
    for(int i=0 ;i<10;i++)
    accept_use(&b1[i]);
}

void accept_use(box_t *b1)
{
    static int number =0;
    printf("enter the box elemnet");
    b1->number = ++number;
    printf("weight : ");
    scanf("%d",&b1->weight);
    printf("height :");
    scanf("%d",&b1->height);
    printf("width   :");
    scanf("%d",&b1->width);
    printf("colour   : " );
    scanf("%s",b1->colour);
}




void print_box(box_t *b1){
    for(int i=0 ;i<10;i++)
    print_use(&b1[i]);
}



void print_use(box_t *b1)
{
 printf("weight : %d\n ",b1->weight);
 printf("height : %d\n ",b1->height);
 printf("width : %d\n ",b1->width);
 printf("colour  : %s\n ",b1->colour);
}


void write_add_record( const char *path){   
    FILE *stream = fopen( path, "wb"); 
    if( stream != NULL ){
        box_t b1;
        accept_box( &b1 );
        fwrite( &b1, sizeof(box_t), 10, stream );
        fclose( stream );
    }else
        printf( "File write operation is failed.\n");
}


void read_record( const char *path){    
    FILE *stream = fopen( path, "rb");
    if( stream != NULL ){
        box_t b1;
        while( fread( &b1, sizeof(box_t), 10, stream ) == 1 ) 
            print_box( &b1);
        fclose( stream );
    }else
        printf(stderr, "File read operation is failed.\n");
}


void delete_box( const char *path, int id ){   
    FILE *stream = fopen( path, "rb"); 
    FILE *fp = fopen( "Temp.dat", "wb"); 
    if( stream != NULL ){
        box_t b1;
        while( fread( &b1, sizeof(box_t), 10, stream ) == 1 ) {
            if( b1.number != id)
                fwrite( &b1, sizeof(box_t), 1, fp );
        }
        fclose( fp );
        fclose( stream );
        remove( path );
        rename( "Temp.dat", path);
    }else
        fprintf(stderr, "File write operation is failed.\n");
}

int** allocate_memory( ){
    int **b = (int**)calloc( 10, sizeof(int*));
    if( b != NULL ){
        for( int index = 0; index < 10; ++ index )
            b[ index ] = (int*)calloc( 10, sizeof(int ) );    
    }
    return b;
}

void free_memory( int **b ){
    for( int index = 0; index < 10; ++ index )
        free( b[ index ] );
    free( b );
    b = NULL;
}
